LCOIN_VERSION=0.18.1
NAME=litecoin
DHUB_USER=leonardocuquejo

build:
	docker build -t ${DHUB_USER}/${NAME}:${LCOIN_VERSION}  -f litecoin/Dockerfile litecoin

run:
	docker run --rm ${DHUB_USER}/${NAME}:${LCOIN_VERSION}

scan:
	docker scan --accept-license --severity medium ${DHUB_USER}/${NAME}:${LCOIN_VERSION}

publish: build scan
	docker push ${DHUB_USER}/${NAME}:${LCOIN_VERSION}